Use Movies
select TOP 10 WITH TIES 
    FilmName AS [title]
	,FilmReleaseDate As [Released on]
	,FilmRunTimeMinutes AS [Duration]
from
     tblFilm
WHERE 
      FilmRunTimeMinutes > 180 AND
	  (FilmName LIKE 's%' OR
	  FilmName LIKE 't%') 
ORDER BY
    FilmReleaseDate