/*
Sprint 1
opdracht 1, SETTING CRITERIA USING WHERE
13 november 2019
*/
USE WorldEvents

SELECT
	EventName AS [naam],
	EventDate AS [datum]
FROM
	tblEvent
WHERE
	CategoryID = 11