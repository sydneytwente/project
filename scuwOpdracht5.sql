/*
sprint 1
opdracht 5, Setting criteria using WHERE
15 november 2019
*/
USE WorldEvents

SELECT
	eventname AS [naam],
	eventDate AS [datum],
	EventDetails AS [details]
FROM
	tblEvent
WHERE
	EventName LIKE '%Teletubbies%' OR
	EventName LIKE '%Pandy%'