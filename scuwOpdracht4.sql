/*
sprint 1
Opdracht 4,  Setting criteria using WHERE
15 november 2019

Events which aren't in the Transport category (number 14),
but which nevertheless include the word Train in the EventDetails column.

Events which are in the Space country (number 13), 
but which don't mention Space in either the event name or the event details columns

Events which are in categories 5 or 6 (War/conflict and Death/disaster), 
but which don't mention either War or Death in the EventDetails column.
*/

USE WorldEvents

SELECT
	EventName AS [naam],
	eventdate AS [wanneer],
	EventDetails AS [details]
FROM
	tblEvent
WHERE
	CategoryID != 14 AND
	EventDetails LIKE '%Train%'

SELECT
	EventName AS [naam],
	eventdate AS [wanneer],
	EventDetails AS [details]
FROM
	tblEvent
WHERE
	CategoryID = 13 AND
	EventDetails NOT LIKE '%Space%'

SELECT
	EventName AS [naam],
	eventdate AS [wanneer],
	EventDetails AS [details]
FROM
	tblEvent
WHERE
	CategoryID = 5 OR
	CategoryID = 6 AND
	(EventDetails NOT LIKE '%Death%' OR
	EventDetails NOT LIKE '%War%')