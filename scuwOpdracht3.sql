/*
sprint 1
opdracht 3, Setting criteria using WHERE
15 november 2019
*/
USE WorldEvents

SELECT
	EventName AS [naam],
	EventDate AS [datum]
FROM
	tblEvent
WHERE
	EventDate between '2005-02-01' and '2005-02-28'