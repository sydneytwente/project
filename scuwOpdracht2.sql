/*
sprint 1
opdracht 2, Setting criteria using WHERE
15 november 2019

Ik mis: They take place in one of the island countries 
(8, 22, 30 and 35, corresponding to Japan, the Marshall Islands, 
Cuba and Sri Lanka respectively)
*/

USE WorldEvents

SELECT
	EventName AS [naam],
	EventDetails AS [details],
	EventDate AS [datum]
FROM
	tblEvent
WHERE
	EventDetails LIKE '%water%' OR
	CategoryID = 4
